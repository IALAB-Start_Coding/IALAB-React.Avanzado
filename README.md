# **Curso IALAB - Start Coding - React.Avanzado**

Curso de Inteligencia Artificial - Start Coding - React.Avanzado

## **Unidad 1: Introducción**

1. Github Repository - Rick & Morty
2. GitHub Reposity
3. Intro teorica
4. Componentes
5. Routing
6. Estados y técnicas de rendering
7. useParams
8. useContext
9. useEffect Desafio terminado

## **Unidad 2: React Lazy & Suspense**

1. Suspens & React,Lazy

## **Unidad 3: Next.JS**

1. Introduccion a Next.js
2. Paginas en Next.js
3. Assets - Refactorizando TS
4. Assets - TS y Context
5. Assets - Imagenes
6. Metadata
7. Styles, CSS Modules , Global Styles
8. Pre rendering y data fetching
9. Pre rendering - Como funciona getStaticProps
10. Pre rendering - GetServerSideProps
11. Pre rendering - Adaptando nuestros Pages
12. Pre rendering - Refactorizando
13. Rutas dinámicas

## **Unidad 4: Graphql**

1. Introducción a GraphQL
2. Instalacion de GraphQL
3. Queries GraphQL
4. Consumiendo Queries
5. Optimizando nuestras queries
6. Paginación en GraphQL
7. Mutations - Teoria
8. Mutation - Creación de interfaces y mutations
9. Mutations - Creación del formulario
10. Mutations - Consumiendo mutations

## **Unidad 5: Jest & React Testing Library**

1. Testing - Instalacion de JEST
2. Testing, casos de prueba - TDD

## **Unidad 6: Manejo de errores**

1. Error Boudary
